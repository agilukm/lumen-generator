# Lumen Codegen

Lumen starter project code generator package

## Installation

Install with composer
`composer require spada/lumen-codegen`

## Register Service Provider

Register your service provider
```php
return [
    'providers' => [
    	// ....
        \Spada\Generator\CodegenServiceProvider::class
    ]
];
```
## Usage

#### Create a migration.

`php artisan make:migration <name>`

#### After setup your migration then migrate.

`php artisan migrate`

#### Create service by codegen command

`php artisan codegen:generate <service_name>`

#### Command for help

`php artisan codegen:generate --help`
