<?php

namespace Spada\Generator\ServiceBuilder;

use Spada\Generator\ServiceBuilder\Contracts\BuilderInterface;
use Illuminate\Support\Str;

class ServiceFactory
{
    protected $serviceName;

    protected $tableName;

    protected $primaryKey;

    protected $class;

    protected $type;

    public function __construct($serviceName, $tableName, $primaryKey)
    {
        $this->serviceName = $serviceName;
        $this->tableName = $tableName;
        $this->primaryKey = $primaryKey;
    }

    protected function isExist($class)
    {
        if (class_exists($class)) {
            return true;
        }
        return $path;
    }

    public function register($class, $type)
    {
        $this->class = $class;
        $this->type = Str::studly($type);

        return $this;
    }

    public function build()
    {
        if (!$this->isExist($this->class)) {
            return "Class Not Found!";
        }

        switch ($this->type) {
            case 'Controller':
                $controller = app()->make($this->class);
                return $controller->setStubPath(__DIR__ . '/../Stubs/controller.stub')
                                  ->setName($this->serviceName)
                                  ->build();
                break;
            case 'Model':
                $model = app()->make($this->class);
                return $model->setStubPath(__DIR__ . '/../Stubs/model.stub')
                             ->setTable($this->tableName)
                             ->setName($this->serviceName)
                             ->setPrimaryKey($this->primaryKey)
                             ->build();
                break;
            case 'Transformer':
                $transformer = app()->make($this->class);
                return $transformer->setStubPath(__DIR__ . '/../Stubs/transformer.stub')
                                   ->setTable($this->tableName)
                                   ->setName($this->serviceName)
                                   ->setPrimaryKey($this->primaryKey)
                                   ->build();
                break;
            case 'Service':
                $repository = app()->make($this->class);
                return $repository->setStubPath(__DIR__ . '/../Stubs/repository.stub')
                                  ->setName($this->serviceName)
                                  ->build();
                break;
            case 'Searchcriteria':
                $repository = app()->make($this->class);
                return $repository->setStubPath(__DIR__ . '/../Stubs/searchcriteria.stub')
                                  ->setName($this->serviceName)
                                  ->build();
                break;
            case 'Route':
                $repository = app()->make($this->class);
                return $repository->setStubPath(__DIR__ . '/../Stubs/route.stub')
                                  ->setName($this->serviceName)
                                  ->build();
                break;
            case 'Request':
                $repository = app()->make($this->class);
                return $repository->setStubPath(__DIR__ . '/../Stubs/request.stub')
                                  ->setTable($this->tableName)
                                  ->setName($this->serviceName)
                                  ->build();
                break;
            case 'Factory':
                $repository = app()->make($this->class);
                return $repository->setStubPath(__DIR__ . '/../Stubs/factory.stub')
                                  ->setTable($this->tableName)
                                  ->setName($this->serviceName)
                                  ->build();
                break;
            case 'Testcase':
                $repository = app()->make($this->class);
                return $repository->setStubPath(__DIR__ . '/../Stubs/testcase.stub')
                                  ->setTable($this->tableName)
                                  ->setName($this->serviceName)
                                  ->build();
                break;

            default:
                return "Type not found!";
                break;
        }

    }

}
