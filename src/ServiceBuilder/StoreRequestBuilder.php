<?php

namespace Spada\Generator\ServiceBuilder;

use Illuminate\Support\Facades\Auth;
use Spada\Core\Requests\FormRequest;
use Spada\Core\Requests\JsonApiRequestTrait;
use Spada\Generator\ServiceBuilder\Contracts\BuilderInterface;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use DB;

class StoreRequestBuilder extends BaseBuilder implements BuilderInterface
{
    protected $stub;

    protected $stubPath;

    protected $name;
    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Request';

    /**
     * Constructor
     *
     * @param  \Illuminate\Filesystem\Filesystem  $files
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    public function setStubPath($stubPath)
    {
        $this->stubPath = $stubPath;
        return $this;
    }

    protected function getStubPath()
    {
        return $this->stubPath;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setTable($tableName)
    {
        $this->tableName = $tableName;
        return $this;
    }

    public function setPrimaryKey($primaryKey)
    {
        $this->primaryKey = $primaryKey;
        return $this;
    }

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function build()
    {
        $className = $this->qualifyClass('Store' . $this->getClassName());

        // get path
        $path = $this->getPath($className);

        // First we will check to see if the class already exists. If it does, we don't want
        // to create the class and overwrite the user's code. So, we will bail out so the
        // code is untouched. Otherwise, we will continue generating this class' files.
        if ($this->alreadyExists($this->getName())) {
            return $this->type . ' already exists!';
        }

        // Next, we will generate the path to the location where this class' file should get
        // written. Then, we will build the class and make the proper replacements on the
        // stub files so that it gets the correctly formatted namespace and class name.
        $this->makeDirectory($path);

        $stub = $this->setStub()
            ->replaceNamespace($className)
            ->replaceClass()
            ->replaceRequestName()
            ->replaceName()
            ->replaceValueFillable()
            ->replacePluralName()
            ->buildStub();

        $this->files->put($path, $stub);

        return $this->type . ' created successfully.';
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function setStub()
    {
        $this->stub = $this->files->get($this->getStubPath());

        return $this;
    }

    /**
     * Replace the fillable dummy for the given stub.
     *
     * @return $this
     */
    public function replaceValueFillable()
    {
        $schema = DB::getDoctrineSchemaManager();
        $columns = collect($schema->listTableDetails($this->tableName)->getColumns());

        $removeFieldList = [
            "ID",  "CREATED_AT", "UPDATED_AT", "DELETED_AT"
        ];

        $fillable = "";
        foreach ($columns as $key => $value) {
            if (!in_array(Str::upper($key), $removeFieldList)) {
                $fillable .= "'data.attributes.{$key}' => 'required' , ";
            }
        }

        $this->stub = str_replace('DummyValueFillable', rtrim($fillable, ",\n"), $this->stub);

        return $this;
    }

    /**
     * Replace the reqyest name dummy for the given stub.
     *
     * @return $this
     */
    protected function replaceRequestName()
    {
        $this->stub = str_replace('DummyRequestName', 'Store' . $this->getRequestName(), $this->stub);
        return $this;
    }

    /**
     * Replace the name dummy for the given stub.
     *
     * @return $this
     */
    protected function replacePluralName()
    {
        $this->stub = str_replace('DummyPluralName', $this->getPluralName(), $this->stub);
        return $this;
    }
    /**
     * Replace the namespace dummy for the given stub.
     *
     * @return $this
     */
    protected function replaceNamespace($namespace)
    {
        $this->stub = str_replace(
            ['DummyNamespace', 'DummyRootNamespace'],
            [$this->getNamespace($namespace), $this->rootNamespace()],
            $this->stub
        );

        return $this;
    }

    /**
     * Replace the name dummy for the given stub.
     *
     * @return $this
     */
    protected function replaceName()
    {
        $this->stub = str_replace('DummyName', Str::studly($this->getName()), $this->stub);
        return $this;
    }

    /**
     * Replace the class name dummy for the given stub.
     *
     * @return string
     */
    protected function replaceClass()
    {
        $class = str_replace($this->getNamespace($this->getName()) . '\\', '', $this->getName());

        $this->stub = str_replace('DummyClass', $class . $this->type, $this->stub);
        return $this;
    }

    protected function buildStub()
    {
        return $this->stub;
    }

    /**
     * Overide Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Http\Requests';
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['resource', null, InputOption::VALUE_NONE, 'Generate a resource Request class.'],
        ];
    }
}
