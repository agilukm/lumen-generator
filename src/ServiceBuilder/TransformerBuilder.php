<?php

namespace Spada\Generator\ServiceBuilder;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use DB;
use Spada\Generator\ServiceBuilder\Contracts\TransformerBuilderInterface;

class TransformerBuilder extends BaseBuilder implements TransformerBuilderInterface
{
    protected $stub;

    protected $stubPath;

    protected $name;

    protected $tableName;

    protected $primaryKey;

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Transformer';


    /**
     * Constructor
     *
     * @param  \Illuminate\Filesystem\Filesystem  $files
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }


    public function setStubPath($stubPath)
    {
        $this->stubPath = $stubPath;
        return $this;
    }

    protected function getStubPath()
    {
        return $this->stubPath;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setStub()
    {
        $this->stub = $this->files->get($this->getStubPath());
        return $this;
    }

    public function setTable($tableName)
    {
        $this->tableName = $tableName;
        return $this;
    }

    public function setPrimaryKey($primaryKey)
    {
        $this->primaryKey = $primaryKey;
        return $this;
    }

    /**
     * Parse the class name and format according to the root namespace.
     *
     * @param  string  $name
     * @return string
     */
    protected function qualifyClass($name)
    {
        return "App\Presenters\\" . $name;
    }

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function build()
    {
        $className = $this->qualifyClass($this->getClassName());

        // get path
        $path = $this->getPath($className);

        // First we will check to see if the class already exists. If it does, we don't want
        // to create the class and overwrite the user's code. So, we will bail out so the
        // code is untouched. Otherwise, we will continue generating this class' files.
        if ($this->alreadyExists($className)) {
            return $this->type . ' already exists!';
        }

        // Next, we will generate the path to the location where this class' file should get
        // written. Then, we will build the class and make the proper replacements on the
        // stub files so that it gets the correctly formatted namespace and class name.
        $this->makeDirectory($path);

        $stub = $this->setStub()
                     ->replaceNamespace($className)
                     ->replaceClass()
                     ->replaceEloquentName()
                     ->replacePluralName()
                     ->replaceProperties()
                     ->buildStub();

        $this->files->put($path, $stub);

        return $this->type . ' created successfully.';
    }

    public function buildStub()
    {
        return $this->stub;
    }

    /**
     * Replace the class name dummy for the given stub.
     *
     * @return $this
     */
    protected function replaceClass()
    {
        $class = str_replace($this->getNamespace($this->getName()) . '\\', '', $this->getName());

        $this->stub = str_replace('DummyClass', $class . $this->type, $this->stub);
        return $this;
    }

    /**
     * Replace the name dummy for the given stub.
     *
     * @return $this
     */
    protected function replaceEloquentName()
    {
        $this->stub = str_replace('DummyEloquentName', $this->getEloquentName(), $this->stub);
        return $this;
    }

    /**
     * Replace the plural name dummy for the given stub.
     *
     * @return $this
     */
    protected function replacePluralName()
    {
        $this->stub = str_replace('DummyPluralName', $this->getPluralName(), $this->stub);
        return $this;
    }

    /**
     * Replace the namespace dummy for the given stub.
     *
     * @return $this
     */
    protected function replaceNamespace($namespace)
    {
        $this->stub = str_replace(
            ['DummyNamespace', 'DummyRootNamespace'],
            [$this->getNamespace($namespace), $this->rootNamespace()],
            $this->stub
        );

        return $this;
    }

    /**
     * Replace the cast dummy for the given stub.
     *
     * @return $this
     */
    public function replaceProperties()
    {
        $schema = DB::getDoctrineSchemaManager();
        $columns = collect($schema->listTableDetails($this->tableName)->getColumns());

        $properties = "";
        foreach ($columns as $key => $column) {
            $properties .= "\t\t\t\t'{$key}' => " . '$model->getAttribute("' . $column->getName() . '"),' . "\n";
        }

        $this->stub = str_replace('DummyProperties', $properties, $this->stub);

        return $this;
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . "\Services\\" . $this->getPluralName();
    }
    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['migration', 'm', InputOption::VALUE_NONE, 'Create a new migration file for the model.'],
        ];
    }

}
