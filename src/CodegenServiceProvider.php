<?php

namespace Spada\Generator;

use Illuminate\Support\ServiceProvider;

/**
 *
 */
class CodegenServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->commands([
            'Spada\Generator\Commands\CodegenCommand'
        ]);
    }
}
