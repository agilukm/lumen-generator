<?php

namespace Spada\Generator\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Schema;
use Spada\Generator\ServiceBuilder\ServiceFactory;
use Spada\Generator\ServiceBuilder\ControllerBuilder;
use Spada\Generator\ServiceBuilder\ModelBuilder;
use Spada\Generator\ServiceBuilder\TransformerBuilder;
use Spada\Generator\ServiceBuilder\RepositoryBuilder;
use Spada\Generator\ServiceBuilder\SearchCriteriaBuilder;
use Spada\Generator\ServiceBuilder\RouteBuilder;
use Spada\Generator\ServiceBuilder\StoreRequestBuilder;
use Spada\Generator\ServiceBuilder\UpdateRequestBuilder;
use Spada\Generator\ServiceBuilder\FactoryBuilder;
use Spada\Generator\ServiceBuilder\TestCaseBuilder;
use Illuminate\Filesystem\Filesystem;

class CodegenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'codegen:generate {name : Service Name}
                            {--force : Replace existing service}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BREAD Generator Command';


    /**
     * handle
     *
     * @return string
     */
    public function handle()
    {
        $name = Str::studly($this->argument('name'));

        if ((! $this->hasOption('force') || ! $this->option('force')) && $this->alreadyExists($name)) {
            $this->error("\n Service already exists!");
            $this->info("\n add --force option for replace existing service.");
            exit;
        }

        // get table name
        $defaultTableName = strtolower(Str::plural($name));
        $tableName = $this->ask('What is your existing table name?', $defaultTableName);
        if (!Schema::hasTable($tableName)) {
            $this->question("Table not found, have you create migration for '$tableName' table and run it?");
            exit(1);
        }

        // get primary key
        $defaultPrimaryKey = 'id';
        $primaryKey = $this->ask("What is primary key of {$tableName} table?", $defaultPrimaryKey);
        if (!Schema::hasColumn($tableName, $primaryKey)) {
            $this->question("Column not found, is '$primaryKey' column exist on '$tableName' table?");
            exit(1);
        }

        // service factory builder
        $factory = new ServiceFactory($name, $tableName, $primaryKey);

        $factory->register(ControllerBuilder::class, "controller")->build();
        $factory->register(TransformerBuilder::class, "transformer")->build();
        $factory->register(RepositoryBuilder::class, "service")->build();
        $factory->register(RouteBuilder::class, "route")->build();
        $factory->register(StoreRequestBuilder::class, "request")->build();
        $factory->register(UpdateRequestBuilder::class, "request")->build();
        $factory->register(FactoryBuilder::class, "factory")->build();
        $factory->register(TestCaseBuilder::class, "testcase")->build();
        $factory->register(ModelBuilder::class, "model")->build();


        $this->info("Cool, service has been generated, and your routes generated on routes/web.php file!\n");

        $this->info($name . " Route Table");
        $serviceName = Str::plural(strtolower($name));
        $headers = ["Method", "URI", "Action"];
        $body = [
            ["GET", "/{$serviceName}", "App\Http\Controllers\{$name}Controller@index"],
            ["POST", "/{$serviceName}", "App\Http\Controllers\{$name}Controller@store"],
            ["GET", "/{$serviceName}/{id}", "App\Http\Controllers\{$name}Controller@show"],
            ["PATCH", "/{$serviceName}/{id}", "App\Http\Controllers\{$name}Controller@update"],
            ["DELETE", "/{$serviceName}/{id}", "App\Http\Controllers\{$name}Controller@delete"],
        ];

        $this->table($headers, $body);

    }

    /**
     * Determine if the class already exists.
     *
     * @param  string  $rawName
     * @return bool
     */
    protected function alreadyExists($serviceName)
    {
        $file = new Filesystem();
        return $file->exists(app()->path() . "/Services/" . Str::studly(Str::plural($serviceName)));
    }
}
